distroQA
-------------

The primary purpose of this tool is to automate OS GUI testing and report issues.

Demo: 
-----

Automated OS testing using Containers:  https://www.youtube.com/watch?v=pTxi-APR3J4
